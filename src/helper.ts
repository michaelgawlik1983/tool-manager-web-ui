import * as React from 'react'

export function arrayToMap<T, K, V>(
    arr: T[],
    key: (item: T) => K,
    value: (item: T) => V): Map<K, V> {
    return new Map<K, V>(arr.map((item) =>
        [key(item), value(item)] as [K, V]))
}

export function mapReactChildren<TProps>(children: React.ReactNode, mapping: (child: React.ReactElement<TProps>) => JSX.Element): JSX.Element[] {
    return React.Children.map(children, mapping)
}
