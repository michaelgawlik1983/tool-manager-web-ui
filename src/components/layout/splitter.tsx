import * as React from 'react'

export const SplitterContainer: React.SFC<{ orientation: "horizontal" | "vertical" }> = (props) =>
    <div className="splitter-container">
        {props.children}
    </div>

export const SplitterPanel: React.SFC<{}> = (props) =>
    <div className="splitter-panel">
        {props.children}
    </div>