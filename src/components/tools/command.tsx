import * as React from 'react'
import * as Config from '../../data/ToolManagerConfig'
import { Icon } from '../common'
import * as CSS from './command.scss'

export const CommandTool: React.SFC<Config.CommandTool & { path: string }> = (props) =>
    <nav className={CSS.nav}>
        <ul>
            {props.items.map(item => buildGroupItem(item, props.path + '_'))}
        </ul>
    </nav>


function buildGroupItem(item: Config.CommandGroupItem, parentPath: string) {
    const path = parentPath + '/' + item.caption

    return <CommandToolItem key={path} item={item} path={path} />
}


const CommandToolItem: React.SFC<{ item: Config.CommandGroupItem, path: string }> = (props) => {

    const { item, path } = props

    return item.type === 'group'
        ? <CommandGroup {...item} path={path} />
        : <Command {...item} />
}

const CommandGroup: React.SFC<{ items: Config.CommandGroupItem[], path: string, caption: string }> = (props) =>
    <li>
        <label>{props.caption}</label>
        <ul>
            {props.items.map(item => buildGroupItem(item, props.path))}
        </ul>
    </li>


const Command: React.SFC<{ caption: string, icon?: string }> = (props) =>
    <li onClick={() => alert(props.caption)}>
        <Icon iconKey={props.icon} />
        <label>{props.caption}</label>
    </li>


