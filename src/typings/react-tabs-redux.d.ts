declare module 'react-tabs-redux' {

    import * as React from 'react'

    interface TabsProps{
        activeLinkStyle?: React.CSSProperties
        visibleTabStyle?: React.CSSProperties
        disableInlineStyles?: boolean
        className?: string
    }

    export class Tabs extends React.Component<TabsProps> {

    }

    interface TabLinkProps{
        to: string
        className?: string
        activeClassName?: string
    }

    export class TabLink extends React.Component<TabLinkProps>{
    }

    interface TabContentProps{
        for: string,
        className?: string
        visibleClassName?: string
    }

    export class TabContent extends React.Component<TabContentProps> {
    }

}