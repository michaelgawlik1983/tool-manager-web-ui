import * as React from 'react'
import * as Config from '../../data/ToolManagerConfig'
import { GridContainer, GridCell, parsePosition } from './grid'
import { TabContainer, TabContainerProps } from './tab'
import { SplitterContainer, SplitterPanel } from './splitter'
import { CommandTool } from './../tools/command'
import { DataFieldTool } from './../tools/datafield'

export class FromConfigVisitor {

    private toolMap: Map<string, JSX.Element>
    private paths: string[]

    constructor() {
        this.paths = []
    }

    private currentPath() {
        return this.paths.join("/")
    }

    public visit(content: Config.Tool | Config.Container): JSX.Element {
        if (typeof content === "undefined") {
            return null
        }
        else {
            this.paths.push(content.type)
            const path = this.currentPath()
            try {
                switch (content.type) {
                    case "grid":
                        return this.visitGrid(content)
                    case "tab":
                        return this.visitTab(content)
                    case "splitter":
                        return this.visitSplitter(content)
                    case "dataField":
                        return <DataFieldTool {...content} path={path} />
                    case "Command":
                        return <CommandTool {...content} path={path} />
                    default:
                        return null    
                }
            } finally {
                this.paths.pop()
            }
        }
    }

    private mapBuckets<TBucket extends Config.Bucket, TRes>(
        buckets: TBucket[],
        mapping: (bucket: TBucket) => TRes) {

        return buckets.map((bucket, index) => {
            this.paths.push(index.toString())
            try {
                return mapping.bind(this)(bucket)
            } finally {
                this.paths.pop()
            }
        })
    }

    visitGrid(layout: Config.GridContainer) {
        return (
            <GridContainer key={this.currentPath()}>
                {
                    this.mapBuckets(layout.cells, cell =>
                        <GridCell position={parsePosition(cell.position)} key={this.currentPath()} caption={cell.caption}>
                            {this.visit(cell.content)}
                        </GridCell>)
                }
            </GridContainer>
        )
    }

    visitTab(layout: Config.TabContainer): JSX.Element {
        return (
            <TabContainer key={this.currentPath()} tabs=
                {
                    this.mapBuckets(layout.tabs, tab => ({
                        caption: tab.caption,
                        path: this.currentPath(),
                        content: this.visit(tab.content)
                    }))
                } >
            </TabContainer>)
    }

    visitSplitter(layout: Config.SplitterContainer): JSX.Element {
        return (
            <SplitterContainer key={this.currentPath()} orientation="horizontal">
                {
                    this.mapBuckets([layout.far, layout.near], bucket =>
                        <SplitterPanel key={this.currentPath()}>
                            {this.visit(bucket.content)}
                        </SplitterPanel>)
                }
            </SplitterContainer>
        )
    }
}