import * as Config from './ToolManagerConfig'

export class ToolManagerRepository {

    static getData(): Config.Container {
        return <Config.Container>require('./DemoToolManagerConfig.json')
    }
}