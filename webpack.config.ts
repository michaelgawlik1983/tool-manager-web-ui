import path from 'path'
import webpack from 'webpack'
import HtmlPlugin from 'html-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import CleanPlugin from 'clean-webpack-plugin'
import WriteFilePlugin from 'write-file-webpack-plugin'


export default <webpack.Configuration>{
  mode: 'development',

  entry: [
    'react-hot-loader/patch',
    './src/index'
  ],

  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: '[name].bundle.js'
  },

  devtool: "inline-source-map",

  devServer: {
    contentBase: './dist',
    port: 1234,
    hot: true
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'css-hot-loader'
          },
          ...ExtractTextPlugin.extract({
            fallback: "style-loader",
            use: [
              {
                loader: 'typings-for-css-modules-loader',
                options: { modules: 'true', camelCase: true, localIdentName: "[name]_[hash:base64]", namedExport: true }
              },
              {
                loader: 'sass-loader'
              }
            ]
          })
        ]
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({ filename: "styles.css" }),
    new CleanPlugin('./dist/'),
    new WriteFilePlugin(),
    new HtmlPlugin({ template: './src/index.html' }),
  ]
}
