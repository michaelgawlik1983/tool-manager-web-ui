import * as React from 'react'
import * as Config from '../../data/ToolManagerConfig'
import * as Grid from '../layout/grid'

import * as CSS from './datafield.scss'

export const DataFieldTool: React.SFC<Config.DataFieldTool & { path: string }> = (props) => {

    return (
        <div className={CSS.datafieldTool}>
            {props.fields.map((fieldConfig, index) => dataField(fieldConfig, props.path + '/' + index + '_'))}
        </div>
    )
}

function dataField(config: Config.AnyDataField, path: string) {
    const styles = getCssStyles(config.position)

    const isMultiLine = config.type === "multiline"

    return [
        <label className={CSS.datafieldToolLabel} style={styles.label} key={path + '/label'} >
            {normalizeCaption(config.caption, isMultiLine)}
        </label>,
        <div style={styles.field} key={path + '/field'}>
            {editor(config)}
        </div>
    ]
}

function editor(config: Config.AnyDataField) {
    switch (config.type) {
        case "bool":
            return <BooleanEditor {...config} />
        case "date":
            return <DateEditor {...config} />
        case "int":
            return <NumberEditor  {...config} />
        case "lookup":
            return <LookupEditor {...config} />
        case "multiline":
            return <MultiLineEditor {...config} />
        case "text":
            return <SingleLineEditor {...config} />
        default:
            return null;
    }
}


function getCssStyles(position: string) {
    var { row, rowSpan, col, colSpan } = Grid.parsePosition(position)
    col = col * 2
    colSpan = colSpan * 2 - 1
    row = row * 2
    rowSpan = rowSpan * 2 - 1

    return {
        label: Grid.positionToCssGridStyle({ row: row - 1, rowSpan: 1, col, colSpan }),
        field: Grid.positionToCssGridStyle({ row, rowSpan, col: col, colSpan: colSpan }),
    }
}

type EditorProps<T> = {
    value: T
}

const BooleanEditor: React.SFC<EditorProps<boolean>> = (props) =>
    <input className={CSS.datafieldToolInput} type="checkbox" />

const SingleLineEditor: React.SFC<EditorProps<string>> = (props) =>
    <input className={CSS.datafieldToolInput} type="text" defaultValue={props.value} />

const LookupEditor: React.SFC<EditorProps<string>> = (props) =>
    <select className={CSS.datafieldToolInput} >
        <option value={props.value}>{props.value}</option>
    </select>

const MultiLineEditor: React.SFC<EditorProps<string>> = (props) =>
    <div className={CSS.datafieldToolMultilineContainer}>
        <textarea className={CSS.datafieldToolMultilineTextarea} defaultValue={props.value} />
    </div>

const NumberEditor: React.SFC<EditorProps<number>> = (props) =>
    <input className={CSS.datafieldToolInput} type="number" defaultValue={formatNumber(props.value)} />

const DateEditor: React.SFC<EditorProps<string>> = (props) =>
    <input className={CSS.datafieldToolInput} type="text" defaultValue={props.value} />

function formatNumber(n: number) {
    return (typeof n === 'number') ? n.toString() : ''
}

function normalizeCaption(caption: string, isMultiLine: boolean) {
    const res = caption.replace(':', '').replace('&', '').replace('_', '\xa0').replace('_', '\xa0')
    if (res.length > 0)
        return res //+ ':'
    else if (isMultiLine)
        return ''
    else
        return '\xa0' //<- non-breakalbe-space, so the label doesn't collapse
}