import * as React from 'react'
import * as CSS from './grid.scss'


export const GridContainer: React.SFC<{}> = (props) =>
    <div className={CSS.layoutGrid}
        style={{ display: 'grid' }}>

        {props.children}
    </div>

export const GridCell: React.SFC<{ caption: string, position: Position }> = (props) =>
    <div className={CSS.layoutGridCell} style={positionToCssGridStyle(props.position)}>
        {props.caption &&
            <label className={CSS.layoutGridCellCaption}>{props.caption}</label>
        }
        {props.children}
    </div>


export type Position = {
    col: number,
    row: number,
    colSpan: number,
    rowSpan: number
}

export function parsePosition(str: string): Position {
    const [__, col, row, colSpan, rowSpan] =
        str.match(/(\d+)\/(\d+)\s+(\d+)x(\d+)/)

    return {
        col: parseInt(col) + 1,
        row: parseInt(row) + 1,
        colSpan: parseInt(colSpan),
        rowSpan: parseInt(rowSpan),
    }
}

export function positionToCssGridStyle(p: Position): React.CSSProperties {
    return {
        gridRow: `${p.row} / span ${p.rowSpan}`,
        gridColumn: `${p.col} / span ${p.colSpan}`,
    }
}
