import * as React from 'react';
import { ToolManagerRepository } from './../data/Repository'
import { FromConfigVisitor } from './layout/fromConfig'

export const App: React.SFC<{}> = (props) => {
    const config = ToolManagerRepository.getData()
    const viewBuilder = new FromConfigVisitor()

    return viewBuilder.visit(config)
}
