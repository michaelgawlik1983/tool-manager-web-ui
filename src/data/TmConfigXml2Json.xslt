<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:tm="http://www.klafka-hinz.de/xml/ns/ui/toolmanager"
    xmlns="">
    
    <xsl:output encoding="UTF-8" method="text" />
    
    <xsl:template match="/tm:ToolManagerConfig">
        {
            layout: <xsl:apply-templates select="tm:Layout/*" />
            tools:[  
            <xsl:apply-templates select="tm:Tools/*" />
            ],
        }
    </xsl:template>
    
    <xsl:template match="tm:Grid">
        {
            type: "Grid",
            cells: [<xsl:apply-templates select="tm:Children" />],
        },
    </xsl:template>

    <xsl:template match="tm:Grid/tm:Children">
        { 
        <xsl:if test="not(exists(@FrameVisibility)) or @FrameVisibility = 'Visible'">
            caption: "<xsl:value-of select="@Title" />",
        </xsl:if>
            position: "<xsl:value-of select="tm:position(.)" />",
            content: <xsl:apply-templates />
        },
    </xsl:template>
    
    <xsl:template match="tm:Tabs">
        { 
            type: "Tab", 
            tabs: [<xsl:apply-templates select="tm:Tab" />],
        },
    </xsl:template>
    
    <xsl:template match="tm:Tab">
        { 
            caption: "<xsl:value-of select="@Title" />",
            content: <xsl:apply-templates />
        },
    </xsl:template>

    <xsl:template match="tm:Splitter">
        { 
            type: "Splitter",
            near: [<xsl:apply-templates select="near" />],
        },
    </xsl:template>
    
    <xsl:template match="tm:Layout//tm:Tool">
        {
            type: "Tool", 
            tool: "<xsl:value-of select="@Name" />",
        },
    </xsl:template>

    <xsl:template match="tm:Tool[@Type='DSBFieldGroup']">
        { 
          type: "DataField", 
          key: "<xsl:value-of select="@Name" />",
          fields: [
        <xsl:for-each select=".//FieldLayoutConfig">
            {
                type: "Text",
                position: "<xsl:value-of select="tm:position(Position)" />",
                label: "<xsl:value-of select="Label"/>",
                field: "<xsl:value-of select="View"/>:<xsl:value-of select="Column"/>",
             },   
        </xsl:for-each>
        ]},
    </xsl:template>
    
    <xsl:template match="tm:Tool[@Type='ViewCommandsTool']">
        { 
            type: "Command", 
            key: "<xsl:value-of select="@Name" />",
            items: [
            <xsl:for-each-group select=".//CommandConfig[IsDisplayed='true' and IsDeleted='false']" group-by="Grouping">
                {
                type: "group",
                caption: "<xsl:value-of select="current-grouping-key()" />",
                items: [
                <xsl:for-each select="current-group()">
                    {
                        type: "cmd",
                        key:  "<xsl:value-of select="Key"/>",
                        caption: "<xsl:value-of select="Caption"/>",
                    },
                </xsl:for-each>
                    ],
                },
            </xsl:for-each-group>
            ],
        },
    </xsl:template>    

    <xsl:function name="tm:position">
        <xsl:param name="node" />
        
        <xsl:value-of select="$node/@Column" />
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$node/@Row" />
        <xsl:text> </xsl:text>
        <xsl:value-of select="($node/@ColumnSpan,1)[1]" />
        <xsl:text>-</xsl:text>
        <xsl:value-of select="($node/@RowSpan,1)[1]" />
    </xsl:function>
    
    <xsl:template match="*" />
    
</xsl:stylesheet>