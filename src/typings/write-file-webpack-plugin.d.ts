declare module 'write-file-webpack-plugin'{

    export = WriteFilePlugIn;

    class WriteFilePlugIn {
        constructor(options?: {
            text?: RegExp,
            useHashIndex?: boolean
        });
    }
    
}