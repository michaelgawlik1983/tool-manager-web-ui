import * as React from 'react'
import * as Config from './../../data/ToolManagerConfig'

export function fromConfig(config: Config.FilterTool, path: string) {
    return (
        <FilterTool key={path}>
            {config.groups.map(group =>
                <FilterGroup caption={group.caption}>
                    {group.filters.map(filter =>
                        <Filter caption={filter.caption} key={filter.key} />)}
                </FilterGroup>
            )}
        </FilterTool>
    )
}

export const FilterTool: React.SFC<{}> = (props) =>
    <div className="filter-tool">
        {props.children}
    </div>

export const FilterGroup: React.SFC<{caption: string}> = (props) =>
    <div className="filter-group">
        {props.children}
    </div>

export const Filter: React.SFC<{caption: string, key: string}> = (props) =>
    <div className="filter" onClick={()=>alert(props.key)}>
        {props.caption}
    </div>

