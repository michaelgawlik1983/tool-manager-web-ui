import * as React from 'react'

import { Tabs, TabLink, TabContent } from 'react-tabs-redux'
import * as CSS from './tab.scss'

import { Icon } from '../common'
import { mapReactChildren } from '../../helper'

export type TabContainerProps = {
    tabs: Array<{
        caption: string,
        icon?: string,
        path: string,
        content: JSX.Element
    }>
}

export const TabContainer: React.SFC<TabContainerProps> = (props) => {
    return (
        <Tabs className={CSS.layoutTabs} disableInlineStyles={true}>

            <div className={CSS.layoutTabsHeaderPanel}>
                
                {props.tabs.map(tab =>
                    <TabLink className={CSS.layoutTabsHeader}
                        activeClassName={CSS.layoutTabsHeaderActive}
                        key={tab.path + '/#header'}
                        to={tab.path}>

                        <Icon iconKey={tab.icon} />
                        {normalizeCaption(tab.caption)}
                    </TabLink>)}
                
            </div>

            {props.tabs.map(tab =>
                <TabContent className={CSS.layoutTabsContent}
                    visibleClassName={CSS.layoutTabsContentVisible}
                    key={tab.path + '/#panel'}
                    for={tab.path}>

                    {tab.content}
                </TabContent>)}
        </Tabs>
    )
}

function normalizeCaption(caption: string) {
    return caption.replace('&', '')
}