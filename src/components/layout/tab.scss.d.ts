export const layoutTabs: string;
export const layoutTabsHeaderPanel: string;
export const layoutTabsHeader: string;
export const layoutTabsHeaderActive: string;
export const layoutTabsContent: string;
export const layoutTabsContentVisible: string;
