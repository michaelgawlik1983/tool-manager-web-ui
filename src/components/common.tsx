import * as React from 'react'

export const Icon: React.SFC<{ iconKey: string }> = (props) =>
    <div className="icon">
        {/*props.iconKey*/}
    </div>

export const Conditional: React.SFC<{ if: any }> = (props) =>
    !!props.if && (props.children as any)    