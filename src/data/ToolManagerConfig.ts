export type Container =
    GridContainer |
    TabContainer |
    SplitterContainer 

export type GridContainer = {
    type: "grid",
    cells: GridBucket[]
}

export type GridBucket = Bucket & {
    position: string
}

export type TabContainer = {
    type: "tab",
    tabs: Bucket[]
}

export type SplitterContainer = {
    type: "splitter",
    near: Bucket
    far: Bucket
}

export type Bucket = {
    caption: string
    content: Container | Tool
}

export type Tool =
    DataFieldTool |
    CommandTool |
    ListTool |
    FilterTool

export type DataFieldTool = {
    type: "dataField",
    key: string,
    fields: AnyDataField[]
}

export type AnyDataField =
    TextDataField |
    LookUpDataField |
    MultilineDataField |
    BoolDataField |
    IntegerDataField |
    NumberDataField |
    DateDataField

export type DataFieldBase={
    caption: string
    position: string
    field: string
    mandatory: boolean
    tooltip: string
}

export type DataField<Type extends string, TValue> = DataFieldBase & {
    type: Type
    value: TValue
}

export type TextDataField = DataField<"text", string>

export type LookUpDataField = DataField<"lookup", string>

export type MultilineDataField = DataField<"multiline", string>

export type BoolDataField = DataField<"bool", boolean>

export type IntegerDataField = DataField<"int", number>

export type NumberDataField = DataField<"int", number>

export type DateDataField = DataField<"date", string>


export type CommandTool = {
    type: "Command",
    key: string,
    items: CommandGroupItem[]
}

export type CommandGroupItem =
    CommandGroup |
    Command

export type CommandGroup = {
    type: "group",
    caption: string,
    items: CommandGroupItem[]
}

export type Command = {
    type: "cmd",
    key: string
    caption: string
    toolTip: string
    icon: string
}

export type ListTool = {
    type: "List",
    key: string,
    data: any[][]
    columns: ListToolColumn[]
}

export type ListToolColumn = {
    caption: string,
    dataType: "Text" | "Number" | "Date"
}

export type FilterTool = {
    type: "Filter",
    key: string,
    groups: FilterGroup[]
}

export type FilterGroup = {
    caption: string
    filters: Filter[]
}

export type Filter = {
    key: string
    caption: string
    toolTip: string
}



export type ContainerVisitor<T=void> = {
    visitGrid: (layout: Container) => T
    visitSplitter: (layout: Container) => T
    visitTab: (layout: Container) => T
}

export type ToolVisitor<T=void> = {
    visitList: (tool: ListTool) => T
    visitDataField: (tool: DataFieldTool) => T
    visitCommand: (tool: CommandTool) => T
    visitFilter: (tool: FilterTool) => T
}

export function visitLayout<T>(layout: Container, visitor: ContainerVisitor<T>): T {
    if (layout.type === "grid")
        return visitor.visitGrid(layout)

    if (layout.type === "splitter")
        return visitor.visitSplitter(layout)

    if (layout.type === "tab")
        return visitor.visitTab(layout)
}

export function visitTool<T>(tool: Tool, visitor: ToolVisitor<T>): T {
    if (tool.type === "List")
        return visitor.visitList(tool)

    if (tool.type === "Command")
        return visitor.visitCommand(tool)

    if (tool.type === "dataField")
        return visitor.visitDataField(tool)

    if (tool.type === "Filter")
        return visitor.visitFilter(tool)
}