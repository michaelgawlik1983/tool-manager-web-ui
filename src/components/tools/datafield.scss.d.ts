export const datafieldTool: string;
export const datafieldToolLabel: string;
export const datafieldToolInput: string;
export const datafieldToolMultilineContainer: string;
export const datafieldToolMultilineTextarea: string;
